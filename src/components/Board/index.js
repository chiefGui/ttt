import React, {Component, PropTypes} from 'react'
import styled from 'styled-components'

import Slot from '../Slot'

import isPlayerOne from '../../engine/isPlayerOne'
import colors from '../../utils/colors'

const DEFAULT_BOARD_SIZE = 9

class Board extends Component {
  static propTypes = {
    plays: PropTypes.array,
    onPlay: PropTypes.func
  }

  render () {
    const {plays, onPlay} = this.props
    const slots = Array(DEFAULT_BOARD_SIZE).fill()
    const Container = styled.div`
      width: 50%;
      text-align: center;
      border: 4px solid ${colors.tertiary.dark};
    `
    const isX = isPlayerOne(plays.length - 1)

    return (
      <Container>
        {slots.map((_, index) => {
          const isSlotPlayed = plays.filter(play => play === index).length > 0

          return (
            <Slot
              key={index}
              onClick={onPlay.bind(null, index)}
            >
              {
                isSlotPlayed
                  ? isX
                    ? 'X'
                    : 'O'
                  : null
              }
            </Slot>
          )
        })}
      </Container>
    )
  }
}

export default Board
