import React, {Component, PropTypes} from 'react'
import styled from 'styled-components'

import colors from '../../utils/colors'

class Slot extends Component {
  static propTypes = {
    isPlayerOne: PropTypes.bool,
    onClick: PropTypes.func
  }

  render () {
    const {isPlayerOne} = this.props
    const Container = styled.div`
      display: inline-block;
      padding: 40px 0;
      width: 33.3333%;
      background-color: ${colors.primary.light};
      cursor: pointer;

      &:nth-child(2n+2) {
        background-color: ${colors.primary.lightest};
      }

      &:hover {
        background-color: ${colors.secondary.medium};
      }
    `
    const Character = styled.div`
      font-size: 4rem;
      color: ${colors.tertiary.dark};
    `

    return (
      <Container onClick={this.props.onClick}>
        <Character>
          {this.props.children}
        </Character>
      </Container>
    )
  }
}

export default Slot
