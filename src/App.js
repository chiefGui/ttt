import React, {Component} from 'react'
import styled from 'styled-components'
import autoBind from 'react-autobind'
// import maxBy from 'lodash/maxBy'
// import size from 'lodash/size'

import Board from './components/Board'

import isPlayerOne from './engine/isPlayerOne'
import isWinner from './engine/isWinner'

import 'normalize.css/normalize.css'

const WINNINGS = [
  // horizontal ->
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],

  // horizontal <-
  [2, 1, 0],
  [5, 4, 3],
  [8, 7, 6],

  // vertical \/
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],

  // vertical /\
  [6, 3, 0],
  [7, 4, 1],
  [8, 5, 2]

  // diagonal \/
  [0, 4, 8],
  [2, 4, 6],

  // diagonal /\
  [8, 4, 0],
  [6, 4, 2]
]

class App extends Component {
  constructor () {
    super ()

    autoBind(this)
  }

  state = {plays: [], winner: null}

  assignSlot (slotNumber) {
    const newState = this.state.plays.concat(slotNumber)
    const winner = this.getWinner(newState)

    this.setState({plays: newState, winner})
  }

  getWinner (newState) {
    const playsByPlayerOne = newState.filter((_, index) => isPlayerOne(index))
    const playsByPlayerTwo = newState.filter((_, index) => !isPlayerOne(index))

    return isWinner(playsByPlayerOne) || isWinner(playsByPlayerTwo)
  }

  renderWinner () {
    const {plays} = this.state
    const lastPlayIndex = plays.length - 1

    return <h1>{isPlayerOne(lastPlayIndex) ? 'Player one won!' : 'Player two won!'}</h1>
  }

  render () {
    const {winner: hasWinner} = this.state
    const Container = styled.div`
      display: flex;
      justify-content: center;
      align-items: center;
      width: 100%;
      height: 100vh;
    `

    return (
      <Container>
        {hasWinner
          ? this.renderWinner()
          : <Board
              plays={this.state.plays}
              onPlay={this.assignSlot}
            />
        }

      </Container>
    )
  }
}

export default App;
