const isPlayerOne = index => index % 2 === 0

export default isPlayerOne
