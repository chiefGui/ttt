import find from 'lodash/find'
import isEqual from 'lodash/isEqual'

const WINNING_PLAYS = [
  // horizontal ->
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],

  // horizontal <-
  [2, 1, 0],
  [5, 4, 3],
  [8, 7, 6],

  // vertical \/
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],

  // vertical /\
  [6, 3, 0],
  [7, 4, 1],
  [8, 5, 2]

  // diagonal \/
  [0, 4, 8],
  [2, 4, 6],

  // diagonal /\
  [8, 4, 0],
  [6, 4, 2]
]

const isWinner = playerPlays => find(WINNING_PLAYS, winningPlay => isEqual(winningPlay, playerPlays))

export default isWinner
