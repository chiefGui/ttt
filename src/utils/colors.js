const colors = {
  primary: {
    lightest: '#DFDCD9',
    light: '#D3D0CB'
  },

  secondary: {
    medium: '#E2C044'
  },

  tertiary: {
    dark: '#393E41'
  }
}

export default colors
